#!/bin/ruby

class Qemu
  attr_writer :verbose, :spice_port, :memory, :maxmem, :smp, :extra, :shared_dir, :monitor,
              :img_file, :img_size, :img_format, :img_snapshot

  def initialize(name)
    @name = name
    @cdrom = ''
    @verbose = false
    @spice_port = 5900
    @memory = 1024
    @maxmem = @memory
    @smp = 'cpus=2,cores=2,threads=1'
    @pidfile = "./vm.#{name}.pid"
    @spicy_pidfile = "./spicy.#{name}.pid"
    @monitor = 1234

    @img_file = "./vm.#{name}.img"
    @img_size = '10G'
    @img_format = 'raw'
    @img_snapshot = 'off'
    @extra = []
  end

  def read_cmd_args(args)
    require 'optparse'
    OptionParser.new do |opts|
      opts.on("-d", "Dry run (don not execut, just simulate)") do |dry_run|
        @dry_run = dry_run
      end
      opts.on("-v", "Verbose output") do |verbose|
        @verbose = verbose
      end
      opts.on("-m", "--memory memory", "VM Memory") do |memory|
        @memory = memory
      end
      opts.on("-r", "--read-only", "Don't change image") do
        @img_snapshot = 'on'
      end
      opts.on("-i", "--image iso") do |image|
        @cdrom = "-cdrom #{image}"
      end
      opts.on("--smp smp", "CPUs") do |smp|
        @smp = smp
      end
      opts.on("-d", "--shared dir", "Spicy shared directory") do |dir|
         @shared_dir = dir
      end
      opts.on("-s", "--spice_port spice_port", "Spice port") do |spice_port|
        @spice_port = spice_port
      end
      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end.parse!(args)
  end


  def create_image
    l_system "qemu-img create -f #{@img_format} #{@img_file} #{@img_size}"
  end

  def image_exists?
    File.file?(@img_file)
  end

  def running? pidfile
    begin
      Process.kill 0, File.read(pidfile).to_i
    rescue Errno::ENOENT
    rescue Errno::ESRCH
      false
    else
      true
    end
  end

  def vm_running?
    running? @pidfile
  end

  def spicy_running?
    running? @spicy_pidfile
  end

  def l_system cmd
    puts "run: #{cmd}" if @verbose
    return system cmd unless @dry_run
    0
  end

  def l_spawn cmd
    puts "run: #{cmd}" if @verbose
    return Process.spawn "#{cmd} >/dev/null" unless @dry_run
    0
  end

  def stop
    if vm_running?
      Process.kill 3, File.read(@pidfile).to_i
    else
      puts "VM '#{name}' is already stopped"
    end
  end

  def run
    create_image unless image_exists?

    if vm_running?
      puts "VM #{@name} is already running..."
    else
      cmd = (
       [
         'qemu-system-x86_64', @cdrom,
         "-name #{@name}",
         "-cpu host",
         "-enable-kvm",
         "-m #{@memory},slots=2,maxmem=#{@maxmem}",
         #"-m #{@memory}",
         "-smp #{@smp}",

         "-curses -monitor telnet:127.0.0.1:#{@monitor},server,nowait",
         "-nographic",
         "-vga qxl",

         # USB Redirection
         "-device ich9-usb-ehci1,id=usb",
         "-device ich9-usb-uhci1,masterbus=usb.0,firstport=0,multifunction=on",
         "-device ich9-usb-uhci2,masterbus=usb.0,firstport=2",
         "-device ich9-usb-uhci3,masterbus=usb.0,firstport=4",
         "-chardev spicevmc,name=usbredir,id=usbredirchardev1",
         "-device usb-redir,chardev=usbredirchardev1,id=usbredirdev1",
         "-chardev spicevmc,name=usbredir,id=usbredirchardev2",
         "-device usb-redir,chardev=usbredirchardev2,id=usbredirdev2",
         "-chardev spicevmc,name=usbredir,id=usbredirchardev3",
         "-device usb-redir,chardev=usbredirchardev3,id=usbredirdev3",

         # Spicy
         "-spice port=#{@spice_port},disable-ticketing=on",
         "-device virtio-serial-pci",
         "-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0",
         #"-device virtserialport,chardev=spicechannel0,name=org.qemu.guest_agent.0",
         "-chardev spicevmc,id=spicechannel0,name=vdagent",
         "-device intel-hda", #-device hda-duplex
         #"-soundhw hda",
         "-pidfile #{@pidfile}",
         "-boot d",
         "-drive \"file=#{@img_file},snapshot=#{@img_snapshot},format=#{@img_format}\""
       ] + @extra
      ).join(' ')
      l_spawn cmd
    end

    if spicy_running?
      puts "Spicy for #{@name} is already running..."
    else
      cmd = [
        'spicy',
        "--host=127.0.0.1",
        "--password=nopass",
      ]
      cmd.append "--port=#{@spice_port}" unless @spice_port.nil?
      cmd.append "--spice-shared-dir=#{@shared_dir}" unless @shared_dir.nil?

      cmd = cmd.join ' '
      puts "run: #{cmd}" if @verbose
      File.open @spicy_pidfile, 'w' do |f|
        f.write Process.spawn cmd
      end unless @dry_run
      0
    end
  end
end

class Win10 < Qemu
  def initialize(args)
    super('Win10')
    @spice_port = 4903
    @memory = 4096
    @img_size = '40G'
    @img_file = './hdd/win10.img'
    @shared_dir = '~/vm/dstr'

    @extra = [
      "-netdev user,id=vmnic",
      "-device virtio-net,netdev=vmnic",
    ]

    read_cmd_args(args)
  end
end

class Arch < Qemu
  def initialize(args)
    super('Arch')
    @spice_port = 4904
    @memory = '3G'
    @maxmem = '5G'
    @img_size = '10G'
    #@img_file = 'hdd.Arch.img'
    @img_format = 'qcow2'
    @img_file = 'tmp/box.img'
    @extra = [
      '-machine pc,accel=kvm,hmat=on',
      '-gdb tcp::3117',
      '-object memory-backend-ram,size=1G,id=m0',
      '-object memory-backend-ram,size=2G,id=m1',
      '-numa node,nodeid=0,memdev=m0',
      '-numa node,nodeid=1,memdev=m1,initiator=0',
      '-numa cpu,node-id=0,socket-id=0',
      #'-numa cpu,node-id=0,sokcet-id=1',
      "-nic user,hostfwd=tcp::8022-:22",

      #"-machine pc,accel=kvm,nvdimm=on",
      #"-object memory-backend-file,id=mem1,share=on,mem-path=./tmp/f27nvdimm0,size=4G,pmem=on",
      #"-device nvdimm,memdev=mem1,id=nv1,label-size=2M",
      #'-object memory-backend-file,id=mem1,share=on,mem-path=./tmp/f27nvdimm0,size=4G',
      #'-device virtio-pmem-pci,memdev=mem1,id=nv1',
      #'-object memory-backend-memfd,id=mem,size=4G', #,share=on
      #'-numa node,memdev=mem'
    ]


    read_cmd_args(args)
  end
end

def usage
  puts "#{$0} - quemu VM wrapper"
  0
end

def main args
  case args.shift
  when 'stop'
    case args.shift
    when 'win10'
        Win10.new(args).stop
    when 'arch'
        Arch.new(args).stop
    end
  when 'win10'
    Win10.new(args).run
  when 'arch'
    Arch.new(args).run
  else
    usage
  end
end

exit main(ARGV)
